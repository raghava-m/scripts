This folder contains various scripts classified into three main categories:
1. analysis - scripts for waveform analysis and parameter estimations
2. matplotlib - illustration and animation scripts using matplotlib
3. jupyter - interactive visualizations using Jupyter Notebooks

Description and usage of scripts in the folders are given below.

# analysis
**fft_plot.m**   
Script for plotting the FFT of a MATLAB workspace variable.   
*Usage:*
Add script folder to MATLAB path and run:
```
fft_plot(var, freq, start_time, sim_step_size, harmonic, norm)
```
For more details run:
```
help fft_plot
```


**motor_params.py**   
Script for calculating 3-phase ACIM parameters
from blocked rotor and no load tests.
The test measurements has to be entered in the script file.   
*Usage:*
Open terminal/command prompt in script folder and run:
```
./motor_params.py
```


# matplotlib
Python scripts using matplotlib and numpy,
sorted into two sub folders - animations and illustrations.
The scripts can be run from a terminal/command prompt or a python IDE (Spyder).

## animations
**cesv.py**   
Script for showing the animation of Current Error Space Vector (CESV),
in a two-level 3-phase inverter.

## illustrations
**singlePhase_PWManalysis.py**   
Script showing harmonic frequencies in single-phase PWM techniques.
Amplitude and frequency modulation indices can be varied to observe how it affects the harmonics.

**threePhase_PWMcarriers.py**   
Script showing the three most popular carrier waves used in 3-phase PWM schemes.
Modulation index can be varied to observe how it affects the carrier waves.

**threePhase_PWMpulses.py**   
Script showing the gate pulse generation in different 3-phase PWM schemes.
Amplitude and angle of the reference voltage can be varied,
to observe their effect on the gate pulses.

**aliasillustraion.py**   
Script illustrating the phenomena of aliasing, when a sine wave is undersampled.


# Jupyter Notebooks
Interactively run visualizations of various concepts from Jupyter Notebooks, by clicking the link below  
[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/opelexnet%2Fsimulation%2Fscripts/master?urlpath=apps%2Fjupyter%2Findex.ipynb)


# todo
Add scripts for photo voltaic panel positioning

# License
All files are made available under MIT License, which can be read from the LICENSE file in this folder.
