#!/usr/bin/env python3
# file: aliasIllustration.py
# Author    : Najath Abdul Azeez
# Copyright : http://opelex.net
# License   : See LICENSE file
"""
Illustration of frequency aliasing due to sampling
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button


# Function for finding the aliased frequency
def alias(freq, fs):
    n   = int((freq + (fs / 2)) / fs)
    fa1 = freq - n * fs
    if (fa1 == -(fs / 2)):
        fa = 0
    else:
        fa = fa1
    print (n, fa1, fa, freq)
    return fa


# Function for updating the waveforms
def update(val):
    fs   = sfs.val
    freq = sfreq.val
    fa   = alias(freq, fs)
    ts   = t[0 : int(1 / res) : int(1 / (res * fs))]      # Sample time array
    l.set_ydata(a0 * np.sin(2 * np.pi * freq * t))
    ls.set_ydata(a0 * np.sin(2 * np.pi * freq * ts))
    ls.set_xdata(ts)
    la.set_ydata(a0 * np.sin(2 * np.pi * fa * t))
    fig.canvas.draw_idle()


# Function for resetting the sliders
def reset(event):
    sfreq.reset()
    sfs.reset()


# Create plot handles
fig, ax = plt.subplots()
# Make space in the bottom, for placing the sliders and button
plt.subplots_adjust(left=0.25, bottom=0.25)

# Define variables and initial values
tmax = 1.0                        # Max time
res  = 1e-5                       # Resolution
t    = np.arange(0.0, 1.0, 1e-5)  # Array for storing time axis values
a0   = 1                          # Amplitude
f0   = 6.00                       # Initial frequency of sampled signal
fs   = 7.0                        # Sampling frequency

# Create the sine wave plot, and get handle to the plot
v  = a0 * np.sin(2 * np.pi * f0 * t)           # Sine wave
l, = plt.plot(t, v, lw=2, color='red')         # Get plot handle

# Plot the sampled values, and get it's handle
ts  = t[0 : int(1 / res) : int(1 / (res * fs))]  # Sample time array
vs  = a0 * np.sin(2 * np.pi * f0 * ts)           # Sample values
ls, = plt.plot(ts, vs, 'ko')                     # Plot & get handle

# Plot the aliased frequency, and get it's handle
fa = alias(f0, fs)
va = a0 * np.sin(2 * np.pi * fa * t)   # Sample values
la, = plt.plot(t, va, lw=2, color='magenta')

# Plot beautification
plt.axis([0, 1, -1.1, 1.1])
axcolor = 'lightgoldenrodyellow'

# Create sliders for sine wave (freq) and sampling frequencies (fs)
axfreq = plt.axes([0.25, 0.10, 0.65, 0.03], facecolor=axcolor)
axfs   = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)
sfreq  = Slider(axfreq,  'Freq', 0.1, 30.0, valinit=f0)
sfs    = Slider(axfs, 'Samples', 1.0, 20.0, valinit=fs)
# Update waveforms on change in freq or fs
sfreq.on_changed(update)
sfs.on_changed(update)

# Create a button for resetting the sliders
resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
button  = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')
# Reset sliders on button click
button.on_clicked(reset)

plt.show()
