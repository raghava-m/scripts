% Author    : Najath Abdul Azeez
% Copyright : http://opelex.net
% License   : See LICENSE file

function fft_plot(var,freq,start_time,sim_step_size,harmonic,norm)
% Description:
% Function for plotting the FFT of a matlab workspace variable,
% which is usually obtained from Simulink 'Scope' or 'To Workspace' blocks.
% Variable has to be in formats 'Array', 'Structure' or 'Structure With Time'.
%
% Usage:
% fft_plot(var,freq,start_time,sim_step_size,harmonic,norm)
%
% Arguments list:
% var           - data to be used for plotting FFT
% freq          - fundamental frequency for calculations
% start_time    - data start time (in seconds) for FFT analysis.
% sim_step_size - simulation step size (in seconds).
% harmonic      - number of harmonics to plot
% norm          - pass nonzero value for plotting normalized FFT.

% Uncomment next line to close all plots, before plotting FFT
% close all

% If the variable is a structure, extract signal array from it
if (isstruct(var))
    v = var.signals(1,1).values;
else
    v = var;
end

% Calculate indices of Start and End samples
start_time_pt = floor(start_time/sim_step_size) + 1;
end_time_pt   = floor((start_time + 1/freq)/sim_step_size);

% Number of data points used for computing FFT
N = end_time_pt - start_time_pt;

% get data of 1 fundamental cycle
v_1cyc = squeeze(v(start_time_pt:end_time_pt));

% Compute FFT
v_FFT = fft(v_1cyc)/N;

% Calculate single-sided amplitude spectrum
h_amp = 2*abs(v_FFT);
h_amp(1) = h_amp(1)/2; % Correct doubling in DC amplitude

% whether to normalize w.r.t fundamental or not
% norm = 1;
if (norm == 0)
   base = 1;
else
   base = h_amp(2);
end

h_norm = h_amp/base;

h_num = 0:1:harmonic;

% Plot FFT as stem
figure;
hL = stem(h_num,h_norm(1:(harmonic+1)),'k');grid on;

% Plot settings
set(hL,'linewidth',1.5)
axis([min(h_num) max(h_num) 0 (max(h_norm)+.1)])
set(gca,'fontsize',16,'fontweight','bold')

% Computation of THD & WTHD
fund_amp = h_amp(2);

harm_squared = 0;
weight_harm_squared = 0;

for i=3:(harmonic+1)
    harm_squared = harm_squared + h_amp(i)*h_amp(i);
    weight_harm_squared = weight_harm_squared + h_amp(i)*h_amp(i)/(i*i);
end

% rms_val=sqrt(h_amp(2)*h_amp(2)+harm_squared);% display(rms_val);
% THD=sqrt( rms_val*rms_val/(fund_amp*fund_amp) -1 );display(THD);
THD=sqrt(harm_squared)/(fund_amp);display(THD);
WTHD=sqrt(weight_harm_squared)/(fund_amp);display(WTHD);

% uncomment to print 1st, 5th & 7th harmonic percentages
% str = sprintf('%5.1f, %5.1f, %5.1f', h_amp(2)*100, h_amp(6)*100, h_amp(8)*100);
% disp(str)

