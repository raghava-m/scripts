#!/usr/bin/env python
# Author    : Najath Abdul Azeez
# Copyright : http://opelex.net
# License   : See LICENSE file
"""
Script for finding motor parameters from
blocked rotor & no load test
"""

from pylab import *


####################################################
# User settings and measurements from motor tests
####################################################
# supply frequency
f = 50

# Readings from blocked rotor test
Vsc   = 127
Isc   = 7.496
pd_sc = 2.88e-3     # phase delay in sec

# Readings from no load test
Vo   = 237
Io   = 1
pd_o = 4.4e-3       # phase delay in sec

# DC resistance
Rs = 4.3 * 1.2      # 1.2 scaling factor for skin effect & heating

####################################################
# Calculations
####################################################

phi_sc = pd_sc * f * 2 * pi   # phase angle in radians
phi_o  = pd_o  * f * 2 * pi   # phase angle in radians

# Blocked rotor power
Wsc = Vsc * Isc * cos(phi_sc)

Rsc = Wsc / (Isc ** 2)
Zsc = Vsc / Isc
Xsc = sqrt((Zsc ** 2) - (Rsc ** 2))
X1  = Xsc / 2
X2_ = X1
Rr_ = Rsc - Rs

Ro = Vo / (Io * cos(phi_o))
Xm = Vo / (Io * sin(phi_o))

Lm = Xm / (2 * pi * f)

Ls = X1  / (2 * pi * f)
Lr = X2_ / (2 * pi * f)

print 'The motor parameters are taken from the python file.'
print '----------------------------------------------------'
print '{:20} {:10.4f}'.format('Lm', Lm)
print '{:20} {:10.4f}'.format('Ls', Ls)
print '{:20} {:10.4f}'.format('Lr', Lr)
print '{:20} {:10.4f}'.format('Rs', Rs)
print '{:20} {:10.4f}'.format('Rr_', Rr_)
print '{:20} {:10.4f}'.format('Rsc', Rsc)
print '{:20} {:10.4f}'.format('Zsc', Zsc)
print '{:20} {:10.4f}'.format('Xsc', Xsc)
print '----------------------------------------------------'
